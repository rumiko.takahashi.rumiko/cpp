#include "demon.h"

int main(void)
{
	if (getuid() == 0)
		demon();
	else
		printf("Usage: sudo ./daemon\n");
}
